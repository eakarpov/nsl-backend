CREATE SCHEMA IF NOT EXISTS nsl_dict;
CREATE EXTENSION IF NOT EXISTS pgcrypto;

CREATE TABLE IF NOT EXISTS nsl_dict.word (
  id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
  value VARCHAR(100) UNIQUE NOT NULL,
  en VARCHAR(100),
  eo VARCHAR(100),
  conj VARCHAR(100),
  ending VARCHAR(100)
);

CREATE TABLE IF NOT EXISTS nsl_dict.word_prefix (
  id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
  word_id UUID REFERENCES nsl_dict.word (id)
)